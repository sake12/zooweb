import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { Animal } from '../shared/animal.model';

@Component({
  selector: 'app-animal-photo',
  templateUrl: './animal-photo.component.html',
  styleUrls: ['./animal-photo.component.css']
})
export class AnimalPhotoComponent {
  @HostBinding('attr.class') cssClass = 'background-color: green';
  @Input() imageUrl: string;
  @Input() animal: Animal;
  @Output() selectedAnimal: EventEmitter<Animal>;

  constructor() {
    this.selectedAnimal = new EventEmitter<Animal>();
  }

  createPath(): string {
    return this.imageUrl || '/assets/images/default.png';
  }

  clicked() {
    this.selectedAnimal.emit(this.animal);
  }
}
