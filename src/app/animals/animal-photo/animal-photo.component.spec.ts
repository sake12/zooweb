import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalPhotoComponent } from './animal-photo.component';

describe('AnimalPhotoComponent', () => {
  let component: AnimalPhotoComponent;
  let fixture: ComponentFixture<AnimalPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
