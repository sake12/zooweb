import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../shared/animal.model';
import { AnimalsService } from '../shared/animals.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-animal-details',
  templateUrl: './animal-details.component.html',
  styleUrls: ['./animal-details.component.css']
})
export class AnimalDetailsComponent implements OnInit {
  animal: Animal;
  animalImage: string;
  animalName: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly service: AnimalsService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getAnimal(params.id);
    });
  }

  async getAnimal(id: string) {
    this.animal = await this.service.download(id).toPromise();
    this.animalImage = this.animal.photoUrl;
    this.animalName = this.animal.name;
  }

  navigateToAnimals() {
    this.router.navigate(['/animals']);
  }

  async updateAnimal() {
    if (!this.isImageEmpty()) {
      this.animal.photoUrl = this.animalImage;
    }

    if (!this.isNameEmpty()) {
      this.animal.name = this.animalName;
    }

    await this.service.update(this.animal).toPromise();
    this.navigateToAnimals();
  }

  isImageEmpty(): boolean {
    return this.animalImage === '';
  }

  isNameEmpty(): boolean {
    return this.animalName === '';
  }
}
