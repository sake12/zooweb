export class Animal {
  id: number;
  name: string;
  classification: string;
  category: string;
  photoUrl: string;
}
