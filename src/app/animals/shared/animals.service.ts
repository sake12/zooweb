import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Animal } from './animal.model';
import { ApiService } from '../../shared/api.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService extends ApiService {
  constructor(httpClient: HttpClient) {
    super(environment.apiUrl, httpClient);
  }

  search(match?: string): Observable<Animal[]> {
    return super.get<Animal[]>(match);
  }

  download(id: string): Observable<Animal> {
    return super.get<Animal>(id);
  }

  remove(animal: Animal): Observable<any> {
    return super.delete<Animal>(`${animal.id}`);
  }

  update(animal: Animal): Observable<any> {
    return super.put(`${animal.id}`, animal);
  }

  add(animal: Animal): Observable<any> {
    return super.post('', animal);
  }
}
