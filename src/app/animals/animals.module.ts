import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnimalsRoutingModule } from './animals-routing.module';
import { AnimalsListComponent } from './animals-list/animals-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AnimalDetailsComponent } from './animal-details/animal-details.component';
import { AnimalPhotoComponent } from './animal-photo/animal-photo.component';


@NgModule({
  declarations: [AnimalsListComponent, AnimalDetailsComponent, AnimalPhotoComponent],
  imports: [
    CommonModule,
    AnimalsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class AnimalsModule { }
