import { Animal } from '../shared/animal.model';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AnimalsService } from '../shared/animals.service';
import { fromEvent, of, Observable, interval } from 'rxjs';
import {
  filter,
  map,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  catchError
} from 'rxjs/operators';

@Component({
  selector: 'app-animals-list',
  templateUrl: './animals-list.component.html',
  styleUrls: ['./animals-list.component.css']
})
export class AnimalsListComponent implements OnInit, AfterViewInit {
  constructor(private readonly service: AnimalsService) {}

  animals: Animal[];
  newAnimalName: string;
  newAnimalcategory: string;
  selectedName: string;
  params = '';
  searchBox: HTMLElement;

  animalTypes: string[] = ['Bird', 'Reptile', 'Amphibian', 'Mammal', 'Fish'];
  selectedType: string;

  ngOnInit(): void {
    this.searchAnimals();
    this.selectedType = 'Bird';
    this.newAnimalName = '';
    this.newAnimalcategory = '';
  }

  ngAfterViewInit(): void {
    this.searchBox = document.getElementById('search-box');

    fromEvent(this.searchBox, 'input')
      .pipe(
        map((e: KeyboardEvent) => {
          const target = e.target as HTMLInputElement;
          return target.value;
        }),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(() =>
          this.searchAnimals$(this.params).pipe(
            catchError(() => {
              return of(new Array<Animal>());
            })
          )
        )
      )
      .subscribe(dd => (this.animals = dd));
  }

  searchAnimals$(params: string) {
    return this.service.search(`?Name=${params}`);
  }

  searchAnimals() {
    this.service
      .search(`?Name=${this.params}`)
      .subscribe(animals => (this.animals = animals));
  }

  async deleteAnimal(animal: Animal) {
    const isConfirmed = confirm('Jesteś tego pewien?');
    if (!isConfirmed) {
      return;
    }

    await this.service.remove(animal).toPromise();
    this.searchAnimals();
  }

  async addAnimal() {
    const animal: Animal = new Animal();
    animal.name = this.newAnimalName;
    animal.category = this.newAnimalcategory;
    animal.classification = this.selectedType;
    animal.photoUrl = '/assets/images/default.png';
    await this.service.add(animal).toPromise();
    this.searchAnimals();
  }

  public cantAdd(): boolean {
    return this.newAnimalName.length > 0 && this.newAnimalcategory.length > 0;
  }

  public highlightRow(animal) {
    this.selectedName = animal.id;
  }

  public animalWasSelected(animal: Animal) {
    console.log(animal);
  }
}
