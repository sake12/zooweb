import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ZooWEB';
  route: string;
  constructor(private router: Router) {
    router.events.subscribe(() => this.route = router.url);
  }
}
